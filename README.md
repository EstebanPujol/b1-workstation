# b1-workstation

# Maîtrise de poste - Day 1

## I. Self-footprinting

### Host OS :

command : `systeminfo`

```
PS C:\WINDOWS\system32> systeminfo

Nom de l’hôte:                              ESTEBAN
Nom du système d’exploitation:              Microsoft Windows 10 Famille
Version du système:                         10.0.19041 N/A version 19041
Fabricant du système d’exploitation:        Microsoft Corporation
[...]
Modèle du système:                          Nitro AN517-51
Type du système:                            x64-based PC
[...]
Mémoire physique totale:                    8 029 Mo
Mémoire physique disponible:                2 804 Mo
Mémoire virtuelle : taille maximale:        14 666 Mo
Mémoire virtuelle : disponible:             4 266 Mo
Mémoire virtuelle : en cours d’utilisation: 10 400 Mo
[...]
Carte(s) réseau:                            2 carte(s) réseau installée(s).
                                            [01]: Realtek Gaming GbE Family Controller
                                                  Nom de la connexion : Ethernet
                                                  État :                Support déconnecté
                                            [02]: Intel(R) Wi-Fi 6 AX200 160MHz
                                                  Nom de la connexion : Wi-Fi
                                                  DHCP activé :         Oui
                                                  Serveur DHCP :        10.33.19.254
                                                  Adresse(s) IP
                                                  [01]: 10.33.19.72
                                                  [02]: fe80::9ae:940:9efb:2624
Configuration requise pour Hyper-V:         Extensions de mode du moniteur d’ordinateur virtuel : Oui
                                            Virtualisation activée dans le microprogramme : Oui
                                            Traduction d’adresse de second niveau : Oui
                                            Prévention de l’exécution des données disponible : Oui
```

```
PS C:\WINDOWS\system32> Get-WmiObject win32_physicalmemory | Format-Table Manufacturer,Banklabel,Configuredclockspeed,Devicelocator,Capacity,Serialnumber -autosize

Manufacturer Banklabel Configuredclockspeed Devicelocator    Capacity Serialnumber
------------ --------- -------------------- -------------    -------- ------------
Samsung      BANK 0                    2667 ChannelA-DIMM0 4294967296 33B6BE5E
Samsung      BANK 2                    2667 ChannelB-DIMM0 4294967296 33B6CD9E
```

```
PS C:\WINDOWS\system32> Get-WmiObject Win32_Processor


Caption           : Intel64 Family 6 Model 158 Stepping 10
DeviceID          : CPU0
Manufacturer      : GenuineIntel
MaxClockSpeed     : 2400
Name              : Intel(R) Core(TM) i5-9300H CPU @ 2.40GHz
SocketDesignation : U3E1
```

```
PS C:\WINDOWS\system32> Get-WmiObject -Class Win32_Processor | Select-Object -Property Name, Number*

Name                                     NumberOfCores NumberOfEnabledCore NumberOfLogicalProcessors
----                                     ------------- ------------------- -------------------------
Intel(R) Core(TM) i5-9300H CPU @ 2.40GHz             4                   4                         8
```

```
PS C:\WINDOWS\system32> wmic path win32_VideoController get name
Name
NVIDIA GeForce GTX 1650
Intel(R) UHD Graphics 630
```

```
PS C:\WINDOWS\system32> wmic diskdrive get Model
Model
Micron_2200_MTFDHBA512TCK
```

```
PS C:\WINDOWS\system32> Get-Partition


   DiskPath : \\?\scsi#disk&ven_nvme&prod_micron_2200_mtfd#4&f929de6&0&010000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

PartitionNumber  DriveLetter Offset                                        Size Type
---------------  ----------- ------                                        ---- ----
1                           1048576                                     100 MB System
2                           105906176                                    16 MB Reserved
3                C           122683392                                475.83 GB Basic
4                           511036096512                                  1 GB Recovery
```

```
PS C:\WINDOWS\system32> Get-Volume

DriveLetter FriendlyName FileSystemType DriveType HealthStatus OperationalStatus SizeRemaining      Size
----------- ------------ -------------- --------- ------------ ----------------- -------------      ----
            Recovery     NTFS           Fixed     Healthy      OK                    533.76 MB   1024 MB
C           Acer         NTFS           Fixed     Healthy      OK                      7.49 GB 475.83 GB
            ESP          FAT32          Fixed     Healthy      OK                     44.72 MB     96 MB
```

```
PS C:\WINDOWS\system32> Get-LocalUser

Name               Enabled Description
----               ------- -----------
Administrateur     False   Compte d’utilisateur d’administration
CDFAccount         False
DefaultAccount     False   Compte utilisateur géré par le système.
esteb              True
Invité             False   Compte d’utilisateur invité
kon-boot           False
WDAGUtilityAccount False   Compte d’utilisateur géré et utilisé par le système pour les scénarios Windows Defender A...
```

```
PS C:\WINDOWS\system32> Get-LocalGroupMember -Group "Administrateurs"

ObjectClass Name                   PrincipalSource
----------- ----                   ---------------
Utilisateur ESTEBAN\Administrateur Local
Utilisateur ESTEBAN\esteb          MicrosoftAccount
Utilisateur ESTEBAN\kon-boot       Local
```

## II. Scripting

```
Write-Output "Nom de la machine : $Env:COMPUTERNAME"

$IpAddress = Get-NetIPAddress -InterfaceAlias Wi-Fi -AddressFamily IPv4 | select IPAddress
Write-Output "IP Principal : $IpAddress"

$OS = (Get-WMIObject win32_operatingsystem).name
Write-Output "OS name : $OS"

$OSVersion = (Get-CimInstance Win32_OperatingSystem).version
Write-Output "OS version : $OSVersion"

$DateHeure = (Get-CimInstance Win32_OperatingSystem).LastBootUpTime
Write-Output "date et heure d'allumage : $DateHeure"

$objComputerSystem = get-wmiObject -class win32_computerSystem
$objOperatingSystem = Get-WmiObject -class Win32_OperatingSystem
"Total Physical Memory (GB) : "+([math]::round([long]$objComputerSystem.TotalPhysicalMemory /(1gb),2))
"Free Physical Memory (GB) : "+([math]::round([long]$objOperatingSystem.FreePhysicalMemory /(1mb),2))
"Total Virtual Memory (GB) : "+([math]::round([long]$objOperatingSystem.TotalVirtualMemorySize /(1mb),2))
"Free Virtual Memory (GB) : "+([math]::round([long]$objOperatingSystem.FreeVirtualMemory /(1mb),2))

$fields = "DeviceID",@{label = "Size (GB)"; Expression = {[math]::round($_.Size / 1gb,2)}},@{label = "FreeSpace (GB)"; Expression = {[math]::round($_.FreeSpace / 1gb,2)}},@{label = "% Occupation"; Expression = {[math]::round(($_.Size-$_.FreeSpace) * 100 / $_.Size,2)}}
Get-WmiObject -Class Win32_LogicalDisk -Filter "DriveType=3" | format-table $fields | Out-String

$listUser = Get-LocalUser | Where-Object {$_.Enabled -eq "True" } | Select name
Write-Output "Liste de tous les utilisateurs :"

Write-Output $listUser
```

```
PS C:\Users\esteb\Desktop\ynov\infra\tp-b1-ws> c:\Users\esteb\Desktop\ynov\infra\tp-b1-ws\test.ps1
Nom de la machine : ESTEBAN
IP Principal : @{IPAddress=192.168.0.34}
OS name : Microsoft Windows 10 Famille|C:\WINDOWS|\Device\Harddisk1\Partition3
OS version : 10.0.19041
date et heure d'allumage : 11/15/2020 15:40:08
Total Physical Memory (GB) : 7.84
Free Physical Memory (GB) : 0.96
Total Virtual Memory (GB) : 16.84
Free Virtual Memory (GB) : 4.49

DeviceID Size (GB) FreeSpace (GB) % Occupation
-------- --------- -------------- ------------
C:          475,83          39,44        91,71
D:            1863        1816,66         2,49



Liste de tous les utilisateurs :

Name
----
esteb
stbAnWork
```

## III. Gestion de softs

```
Installation, mise à jour, et désinstallation ;
Utilisation des paquets provenant de supports variés (CD d'installation, dépôts sur internet, partage réseau…) ;
Vérification des sommes de contrôle de chaque paquet récupéré pour en vérifier l'intégrité ;
Vérification des dépendances logicielles afin d'obtenir une version fonctionnelle d'un paquetage ;
```
```

```


## Machine virtuelle

```
[root@localhost ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:a1:14:b9 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 84493sec preferred_lft 84493sec
    inet6 fe80::d1ed:2733:9720:2d06/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:14:59:62 brd ff:ff:ff:ff:ff:ff
    inet 192.168.120.50/24 brd 192.168.120.255 scope global noprefixroute dynamic enp0s8
       valid_lft 499sec preferred_lft 499sec
    inet6 fe80::3746:b16e:94fe:d465/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

[root@localhost ~]# ifup enp0s8
```

```
C:\Users\esteb>cd..

C:\Users>cd..

C:\>md PcEsteban

C:\>cd PcEsteban

C:\PcEsteban>type nul > test.txt
```

```
[root@localhost ~]# yum install -y cifs-utils

[root@localhost ~]# mkdir /opt/partage

[root@localhost ~]# mount -t cifs -o username=stbAnWork,password=******* //192.168.120.1/PcEsteban /opt/partage

[root@localhost ~]# cd opt

[root@localhost ~]# cd partage

[root@localhost ~]# ls
test.txt

[root@localhost ~]# touch test1.txt

[root@localhost ~]# ls
test1.txt test.txt

[root@localhost ~]# rm test1.txt

[root@localhost ~]# ls
test.txt


[root@localhost ~]# vi test.txt
```
